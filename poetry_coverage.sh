#! /bin/sh

poetry run pytest --cov --cov-report xml:/app/coverage/coverage.xml --cov-report=term
