import requests
import os
from todo_app.data import Item
from azure.keyvault.secrets import SecretClient
from azure.identity import DefaultAzureCredential, ManagedIdentityCredential


def fetch_secrets():
    """
    Fetches secrets from Vault

    Returns
      dictionary: Trello secrets
    """
    if (str(os.getenv('APP_LAUNCH')) == "test") or (str(os.getenv('APP_LAUNCH')) == "local"):
      key=str(os.getenv('KEY'))
      token=str(os.getenv('TOKEN'))
      board = os.getenv('BOARD')
    else:
      keyVaultName = str(os.getenv('VAULT_NAME'))
      KVUri = f"https://{keyVaultName}.vault.azure.net"
      client_id = str(os.getenv('AZURE_MANAGED_IDENTITY'))
      credential = ManagedIdentityCredential(client_id=client_id)
      client = SecretClient(vault_url=KVUri, credential=credential)
      key = client.get_secret("KEY").value
      token = client.get_secret("TOKEN").value
      board = client.get_secret("BOARD").value
 
    secrets = {'API_KEY':key, 'API_TOKEN':token, 'BOARD_ID':board}
    return secrets


def get_items():
    """
    Fetches all saved items from Trello.

    Returns:
        list: The list of Trello items.
    """
    secrets = fetch_secrets()
    key=secrets.get('API_KEY')
    token=secrets.get('API_TOKEN')
    board = secrets.get('BOARD_ID')
    
    url = f'https://api.trello.com/1/boards/{board}/lists?cards=open&key={key}&token={token}'
    
    
    response = requests.get(url=url)
    responseList = response.json()
    cards=[]
    for item in responseList:
      for card in item['cards']:
        myCard = Item.Item.from_trello_card(card,item)
        cards.append(myCard)
    
    return cards
def get_lists():
    """
    Fetches the the lists from Trello.

    Returns:
        list: The list of Trello Lists.
    """
    secrets = fetch_secrets()
    key=secrets.get('API_KEY')
    token=secrets.get('API_TOKEN')
    board = secrets.get('BOARD_ID')
    url = "https://api.trello.com/1/boards/"+board+"/lists?key="+key+"&token="+token

    response = requests.get(url=url)
    responseList = response.json()
    return responseList

def get_item(id):
    """
    Fetch a single card from Trello

    Returns:
        Dictionary: The Trello card requested.
    """
    secrets = fetch_secrets()
    key=secrets.get('API_KEY')
    token=secrets.get('API_TOKEN')
    board = secrets.get('BOARD_ID')
    url = "https://api.trello.com/1/cards/"+id+"?key="+key+"&token="+token

    response = requests.get(url=url)
    card = response.json()
    return card

def save_item(item):
    """
    Updates a Trello card

   
    """
    url = "https://api.trello.com/1/cards/"+item['id']
    secrets = fetch_secrets()
    key=secrets.get('API_KEY')
    token=secrets.get('API_TOKEN')
    board = secrets.get('BOARD_ID')
    headers = {
      "Accept": "application/json"
    }

    query = {
      'key': key,
      'token': token,
      'name' : item['name'],
      'idList' : item['status'],
      'desc' : item['desc']
    }

    response = requests.request(
      "PUT",
      url,
      headers=headers,
      params=query
    )
    

def delete_item(id):
    """
    Deletes a Trello card

   
    """
    url = "https://api.trello.com/1/cards/"+id
    secrets = fetch_secrets()
    key=secrets.get('API_KEY')
    token=secrets.get('API_TOKEN')
    board = secrets.get('BOARD_ID')
    query = {
      'key': key,
      'token': token,
      }

    response = requests.request(
      "DELETE",
      url,
      params=query
    )
    print (response.text)

def add_item(item):
    """
    Adds a Trello card

   
    """
    url = "https://api.trello.com/1/cards"
    headers = {
      "Accept": "application/json"
    }
    secrets = fetch_secrets()
    key=secrets.get('API_KEY')
    token=secrets.get('API_TOKEN')
    board = secrets.get('BOARD_ID')
    query = {
      'key': key,
      'token': token,
      'name' : item['name'],
      'idList' : item['idList'],
      'desc' : item['desc']
      }

    response = requests.request(
      "POST",
      url,
      params=query
    )
    print (response.text)
