class Item:
    def __init__(self, id, name, status,desc):
        self.id = id
        self.name = name
        self.status = status
        self.desc = desc

    @classmethod
    def from_trello_card(cls, card):
        return cls(card['_id'], card['name'], card['status'],card['desc'])