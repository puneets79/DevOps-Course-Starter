import requests
from todo_app.data import mongo_items
def migrate_items(key,token,board):
    """
    Imports all items from Trello

    """
    
    imported = mongo_items.check_imported(board)
    if (imported == False):
        url = f'https://api.trello.com/1/boards/{board}/lists?cards=open&key={key}&token={token}'
        response = requests.get(url=url)
        responseList = response.json()
        cards=[]
        for item in responseList:
            for card in item['cards']:
                myCard = {"name":card['name'], "desc":card['desc'], "status":item['name']}
                mongo_items.add_item(myCard)
        mycard = {"name":"Import Trello", "desc":board, "status":"imported"}
        mongo_items.add_item(mycard)