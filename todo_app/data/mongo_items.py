import requests
import pymongo
import os
from todo_app.data import Item

def check_imported(board):
    """
    Checks if the board was imported

    Returns:
        list: The list of TODO items.
    """
    client = pymongo.MongoClient(os.getenv("DB_STRING"))
    database = client["todoapp"]
    collection = database['todoapp_table']
    cursor = collection.find({"desc":board,"status":"imported"})
    imported=False
    for document in cursor:
        imported = True
    return imported

def get_items():
    """
    Fetches all saved TODO items from Mongo.

    Returns:
        list: The list of TODO items.
    """
    client = pymongo.MongoClient(os.getenv("DB_STRING"))
    database = client["todoapp"]
    collection = database['todoapp_table']
    cursor = collection.find({})
    cards = []
    for document in cursor:
        card = Item.Item.from_trello_card(document)
        cards.append(card)
    return cards

def get_lists():
    """
    Fetches the the listsof statuses allowed

    Returns:
        list: The list of statuses
    """
    responseList = [{'id':"To do",'name':"To do"},{'id':"Doing",'name':"Doing"},{'id':"Done",'name':"Done"}]
    return responseList

def get_item(id):
    """
    Fetch a single item from Mongo

    Returns:
        Dictionary: The TODO card requested.
    """
    client = pymongo.MongoClient(os.getenv("DB_STRING"))
    database = client["todoapp"]
    collection = database['todoapp_table']
    cursor = collection.find({'_id':id})
    for document in cursor:
        card = Item.Item.from_trello_card(document)
    return card

def save_item(item):
    """
    Updates a TODO card in the database

   
    """ 
    client = pymongo.MongoClient(os.getenv("DB_STRING"))
    database = client["todoapp"]
    collection = database['todoapp_table']
    old_values = {"_id":item.id}
    new_value = {"$set":{'name':item.name, 'desc':item.desc, 'status':item.status}}  
    collection.update_one(old_values,new_value)

def delete_item(id):
    """
    Deletes a TODO card from database

   
    """
    client = pymongo.MongoClient(os.getenv("DB_STRING"))
    database = client["todoapp"]
    collection = database['todoapp_table']
    old_values = {"_id":id}
    collection.delete_one(old_values)

def add_item(item):
    """
    creates a TODO card in the database

   
    """
    client = pymongo.MongoClient(os.getenv("DB_STRING"))
    database = client["todoapp"]
    collection = database['todoapp_table']
    collection.insert_one(item)
    
    
