
from flask import Flask,render_template, request, redirect, url_for
from todo_app.flask_config import Config
from todo_app.data import  ViewModel, mongo_items, Item, migrate
import operator
def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())


    @app.route('/')
    def index():
        cards = mongo_items.get_items()
        view_model = ViewModel.ViewModel(cards)
        return render_template('index.html', viewmodel=view_model, route='display')

    @app.route('/add', methods=['GET','POST'])
    def addtodo():
        if (request.method == 'GET'):
            return render_template('add.html',statuses=mongo_items.get_lists())
        else:
            item = {}
            item['name'] = request.form.get('taskname')
            item['status'] = request.form.get('status')
            item['desc'] = request.form.get('taskdesc')
            mongo_items.add_item(item)
            return redirect('/')
        
    @app.route('/update', methods=['GET','POST'])
    def updatetodo():
        if (request.method == 'GET'):
            id = request.values.get("id","")
            item = mongo_items.get_item(id)
            statusList = mongo_items.get_lists()
            return render_template('update.html', item = item, statuses = statusList)
        else:
            id = request.form.get('id')
            taskname = request.form.get('taskname')
            status = request.form.get('status')
            desc = request.form.get('taskdesc')
            card = {'_id':id, 'name':taskname, 'status':status, 'desc':desc}
            item = Item.Item.from_trello_card(card)
            print (item)
            mongo_items.save_item(item)
            return redirect('/')
    
    @app.route('/migrate', methods=['GET','POST'])
    def migratetodo():
        if (request.method == 'GET'):
            return render_template('migrate.html')
        else:
            key = request.form.get('apikey')
            token = request.form.get('apitoken')
            board = request.form.get('board')
            migrate.migrate_items(key,token,board)
            return redirect('/')
        
    @app.route('/delete')
    def deletetodo():
        id = request.values.get("id","")
        mongo_items.delete_item(id)
        return redirect('/')
    
    return app
    