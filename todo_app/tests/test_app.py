import os
from dotenv import load_dotenv, find_dotenv
from flask.testing import FlaskClient
import pytest
import pymongo
from todo_app import app

import mongomock

@pytest.fixture
def client():
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)
    
    test_app = app.create_app()
    with test_app.test_client() as client:
         yield client

def test_index_page(client: FlaskClient):
    # This replaces any call to requests.get with our own function
    card = {'name': 'Test card', 'desc': 'Test Card', 'status': 'To do'}
        
    
    mongoclient = pymongo.MongoClient(os.getenv('DB_STRING'))
    database = mongoclient["todoapp"]
    collection = database['todoapp_table']
    collection.insert_one(card)
    response = client.get('/')
    print (response.data.decode())
    assert response.status_code == 200
    assert 'Test card' in response.data.decode()

